## Introduction

Software repository for the model optimisation package as described in the preprint

Philippe A Robert, Henrik Jönsson, Michael Meyer-Hermann (2018)
MoonFit, a minimal interface for fitting ODE dynamical models, bridging simulation 
by experimentalists and customization by C++ programmers. bioRxiv doi: https://doi.org/10.1101/281188

## Code

This repository is a fork from the main repository https://gitlab.com/Moonfit/MoonLight


"MoonLight is the curated/last version of Moonfit. This is a set of C++ classes to perform simulations and optimizations of ODE models, together with a user-friendly graphical class.
More information can be found on BioRxiv:
biorxiv.org/content/early/2018/03/13/281188
More information about installation can be found in the supplementary material."

